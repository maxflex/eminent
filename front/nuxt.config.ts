import vuetify from 'vite-plugin-vuetify'

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  css: ['@/styles/index.scss'],

  vite: {
    // @ts-expect-error
    // curently this will lead to a type error, but hopefully will be fixed soon #justBetaThings
    ssr: {
      noExternal: ['vuetify'], // add the vuetify vite plugin
    },
    css: {
      preprocessorOptions: {
        scss: {
          api: 'modern-compiler',
        },
      },
    },
  },

  runtimeConfig: {
    public: {
      baseUrl: '',
    },
  },

  app: {
    head: {
      title: null,
      meta: [
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=400, user-scalable=no' },
        { hid: 'description', name: 'description', content: '' },
        { name: 'format-detection', content: 'telephone=no' },
      ],
    },
  },

  modules: [
    '@vite-pwa/nuxt',
    '@pinia/nuxt',
    '@nuxt/eslint',
    // @ts-expect-error
    // this adds the vuetify vite plugin
    // also produces type errors in the current beta release
    async (options, nuxt) => {
      nuxt.hooks.hook('vite:extendConfig', config =>
        config.plugins.push(vuetify()))
    },
  ],

  eslint: {
    config: {
      stylistic: true,
    },
  },

  pwa: {
    registerType: 'autoUpdate',
    manifest: {
      name: 'Eminent',
      short_name: 'Eminent',
      description: 'Eminent',
      display: 'standalone',
      theme_color: '#f1e4a5',
      background_color: '#ffffff',
      icons: [
        {
          src: 'pwa-64x64.png',
          sizes: '64x64',
          type: 'image/png',
        },
        {
          src: 'pwa-192x192.png',
          sizes: '192x192',
          type: 'image/png',
        },
        {
          src: 'pwa-512x512.png',
          sizes: '512x512',
          type: 'image/png',
        },
        {
          src: 'pwa-512x512.png',
          sizes: '512x512',
          type: 'image/png',
          purpose: 'any',
        },
        {
          src: 'pwa-512x512.png',
          sizes: '512x512',
          type: 'image/png',
          purpose: 'maskable',
        },
      ],
      lang: 'ru',
      orientation: 'portrait-primary',
    },
    workbox: {
      navigateFallback: undefined,
      globPatterns: ['**/*.{js,css,ttf,svg,jpg}'],
    },
  },

  compatibilityDate: '2024-10-31',
})
