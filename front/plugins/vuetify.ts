import type { ThemeDefinition } from 'vuetify'
import { createVuetify } from 'vuetify'

const myTheme: ThemeDefinition = {
  dark: false,
  colors: {
    'background': '#ffffff',
    'surface': '#ffffff',
    'primary': '#f1e4a5',
    'accent': '#f47fcc',
    'secondary': '#008ecb',
    'error': '#ff7134',
    'info': '#2196f3',
    'success': '#4caf50',
    'warning': '#ff4600',
    'orange': '#ff7134',
    'grey': '#9e9e9e',
    'grey-light': '#b6b6b6',
    'on-surface': '#414141',
  },
}

export default defineNuxtPlugin((nuxtApp) => {
  const vuetify = createVuetify({
    defaults: {
      VBtn: {
        variant: 'flat',
        ripple: false,
      },
      VSelect: {
        variant: 'outlined',
        hideDetails: true,
      },
      VTextField: {
        variant: 'outlined',
        hideDetails: true,
      },
      VTextarea: {
        variant: 'outlined',
        hideDetails: true,
      },
      VCheckbox: {
        falseIcon: 'jo-basic-circle',
        trueIcon: 'jo-basic-circle-checked',
        hideDetails: true,
        ripple: false,
        // на самом деле null не существует, но WARN пропадает
        density: null,
      },
    },
    theme: {
      defaultTheme: 'myTheme',
      themes: {
        myTheme,
      },
    },
  })
  nuxtApp.vueApp.use(vuetify)
})
