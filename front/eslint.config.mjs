import antfu from '@antfu/eslint-config'

export default antfu({
  formatters: true,
  vue: true,
}).overrideRules({
  'no-console': 'off',
  'no-alert': 'off',
  'ts/ban-ts-comment': 'off',
  'eslint-comments/no-unlimited-disable': 'off',
  'no-case-declarations': 'off',
  'no-fallthrough': 'off',
  // '@typescript-eslint/ban-ts-comment': 'off',
  // '@typescript-eslint/no-dynamic-delete': 'off',
  // 'vue/no-multiple-template-root': 'off',
})
