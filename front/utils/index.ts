import type { Dayjs } from 'dayjs'

export const counts = ref<Counts>({
  plans: 0,
  habits: 0,
  shoppingLists: 0,
})

export async function updateCounts() {
  const { data } = await useHttp<Counts>(`counts`)
  counts.value = data.value!
}

export function plural(value: number, words: string[], withValue: boolean = true) {
  return (withValue ? `${value} ` : '')
    + words[
      value % 100 > 4 && value % 100 < 20
        ? 2
        : [2, 0, 1, 1, 1, 2][value % 10 < 5 ? Math.abs(value) % 10 : 5]
    ]
}

export function rub(value: number | null) {
  return value === null
    ? ''
    : `${value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ')} ₽`
}

export function formatDate(value: string) {
  const { $dayjs } = useNuxtApp()
  return $dayjs(value).format('DD.MM.YYYY')
}

export function dateTime(value: string | Dayjs) {
  const { $dayjs } = useNuxtApp()
  return $dayjs(value).format('DD MMM в HH:mm')
}

export function daysAsText(days: number, zeroLabel: string = 'сегодня') {
  if (days === 0) {
    return zeroLabel
  }
  const months = Math.floor(days / 30)
  if (months >= 11) {
    return 'год'
  }
  if (months >= 6) {
    return 'полгода'
  }
  if (months > 0) {
    return plural(months, ['месяц', 'месяца', 'месяцев'], months > 1)
  }
  if (days % 7 === 0) {
    return plural(days / 7, ['неделю', 'недели', 'недель'], days > 7)
  }
  return plural(days % 30, ['день', 'дня', 'дней'])
}

export function howLongAgo(value: string, exact: boolean = false) {
  const { $dayjs, $today } = useNuxtApp()
  const date = $dayjs(value)
  const dateFormatted = date.format('YYYY-MM-DD')
  if (dateFormatted === $today) {
    return date.format('сегодня')
  }
  if (dateFormatted === $dayjs().subtract(1, 'day').format('YYYY-MM-DD')) {
    return date.format('вчера')
  }
  if (date.diff($today, 'week') === 0) {
    return 'на этой неделе'
  }
  const monthsAgo = $dayjs($today).diff(date, 'month')
  if (monthsAgo === 0) {
    return 'в этом месяце'
  }
  if (monthsAgo < 12) {
    return (
      `${plural(monthsAgo, ['месяц', 'месяца', 'месяцев'], monthsAgo > 1)
      } назад`
    )
  }
  if (monthsAgo) {
    return
  }
  return exact ? date.format('DD.MM.YY') : 'уже давно'
}

export function selectItems<T extends { [key: string]: string }>(items: T) {
  return Object.keys(items).map(key => ({
    value: key as keyof T,
    title: items[key as keyof T],
  }))
}

export function highlight(entity: string, id: number, className: 'item-updated' | 'item-deleted') {
  nextTick(() => {
    const el = document?.querySelector(`#${entity}-${id}`)
    el?.scrollIntoView({ block: 'center', behavior: 'smooth' })
    el?.classList.remove(className)
    setTimeout(() => el?.classList.add(className), 0)
  })
}

export function itemUpdated(entity: string, id: number) {
  highlight(entity, id, 'item-updated')
}
