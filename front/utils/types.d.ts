declare global {
  interface Meta {
    current_page: number
    last_page: number
  }

  interface Finishable {
    id: number
    is_important: boolean
    is_finished: boolean
    title: string
  }

  type UpdateType = 'created' | 'updated' | 'deleted'

  interface ApiResponse<T> {
    data: T[]
    meta: Meta
  }

  interface Penalty {
    title: string
    cnt: number
    sum: number
  }

  interface Plan extends Finishable {
    date: string
    comment: ?string
    time?: string
    created_at?: string
  }

  interface User {
    id: number
    debt: number
  }

  interface Pleasure {
    id?: number
    title: string
    units: string
    comment: string
    penalty: number | null
    cooldown: number | null
    is_hidden: boolean
    current_streak: number | null
    max_streak: number | null
    last_used_at?: string
    last_free_used_at?: string
    is_free_available: boolean
    is_streak_record: boolean
  }

  interface MenuItem {
    to: string
    icon: string
    count?: keyof Counts
  }

  interface ShoppingList extends Finishable {
    comment: ?string
    priority: number
    is_archived: boolean
  }

  type HabitPeriod = keyof typeof HabitPeriodLabel

  interface HabitResource extends Finishable {
    period: HabitPeriod
    daily_on: Weekday[]
    created_at?: string
  }

  interface HabitForTodayResource extends Finishable {
    date: string
    period: HabitPeriod
    daily_on: Weekday[]
    streak: {
      current: number
      ended: number
    }
  }

  interface Counts {
    plans: number
    habits: number
    shoppingLists: number
  }

  type Weekday = keyof typeof WeekdayLabel
}

export {}
