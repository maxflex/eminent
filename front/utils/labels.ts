export const HabitPeriodLabel = {
  daily: 'по дням',
  weekly: 'раз в неделю',
  monthly: 'раз в месяц',
} as const

export const WeekdayLabel = {
  0: 'пн',
  1: 'вт',
  2: 'ср',
  3: 'чт',
  4: 'пт',
  5: 'сб',
  6: 'вс',
} as const

export const WeekdayDativeLabel = {
  0: 'понедельникам',
  1: 'вторникам',
  2: 'средам',
  3: 'четвергам',
  4: 'пятницам',
  5: 'субботам',
  6: 'воскресеньям',
} as const
