<?php

use App\Http\Controllers\{AuthController,
    CalendarController,
    CountsController,
    HabitController,
    HabitForTodayController,
    PaymentController,
    PenaltyController,
    PlanController,
    PleasureController,
    ShoppingListController};
use Illuminate\Support\Facades\Route;

Route::controller(AuthController::class)->prefix('auth')->group(function () {
    Route::post('login', 'login');
    Route::get('user', 'user');
});

Route::apiResource('calendar', CalendarController::class)->only('index');
Route::get('plans/get-badge', [PlanController::class, 'getBadge']);
Route::get('counts', CountsController::class);

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('plans/events', [PlanController::class, 'events']);
    Route::apiResource('penalties', PenaltyController::class)->only('index', 'store');
    Route::apiResource('payments', PaymentController::class)->only('store');
    Route::apiResources([
        'plans' => PlanController::class,
        'habits' => HabitController::class,
        'habits-for-today' => HabitForTodayController::class,
        'pleasures' => PleasureController::class,
        'shopping-list' => ShoppingListController::class,
    ]);
});
