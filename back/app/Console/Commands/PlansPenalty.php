<?php

namespace App\Console\Commands;

use App\Models\Plan;
use Illuminate\Console\Command;

class PlansPenalty extends Command
{
    protected $signature = 'plans:penalty';

    protected $description = 'Штраф за невыполненные планы с пометкой "точно сделаю"';

    /**
     * Запускается каждый день в 5 утра
     */
    public function handle(): void
    {
        $plans = Plan::query()
            ->where('date', now()->modify('-1 day'))
            ->whereNotNull('is_important')
            ->where('is_finished', 0)
            ->get();

        foreach ($plans as $plan) {
            $plan->penalties()->create([
                'date' => now(),
                'title' => 'Планы',
                'penalty' => 3000,
            ]);
        }
    }
}
