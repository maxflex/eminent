<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PleasureRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'title' => ['required'],
            'penalty' => ['required', 'numeric', 'min:50'],
        ];
    }
}
