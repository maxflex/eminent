<?php

namespace App\Http\Controllers;

use App\Http\Resources\ShoppingListResource;
use App\Models\ShoppingList;
use Illuminate\Http\Request;

class ShoppingListController extends Controller
{
    public function index(Request $request)
    {
        $query = ShoppingList::active()
            ->orderByRaw("
                is_finished asc,
                `priority` desc,
                updated_at desc,
                id desc
            ");
        $this->filter($request, $query);
        return $this->handleIndexRequest($request, $query, ShoppingListResource::class);
    }

    public function store(Request $request)
    {
        $request->validate(['title' => 'required']);
        $shoppingList = ShoppingList::create($request->all());
        return new ShoppingListResource($shoppingList);
    }

    public function update(ShoppingList $shoppingList, Request $request)
    {
        $shoppingList->fill($request->all());
        $shoppingList->setUpdatedAt(now());
        $shoppingList->save();
        return new ShoppingListResource($shoppingList);
    }

    public function destroy(ShoppingList $shoppingList)
    {
        $shoppingList->delete();
    }
}
