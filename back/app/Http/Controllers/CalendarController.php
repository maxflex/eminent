<?php

namespace App\Http\Controllers;

use App\Models\Plan;
use DateTime;
use Spatie\IcalendarGenerator\Components\{Calendar, Event};
use Spatie\IcalendarGenerator\Enums\EventStatus;

class CalendarController extends Controller
{
    public function index()
    {
        $calendar = Calendar::create(config('app.name'));
        $plans = Plan::get();
        foreach ($plans as $plan) {
            $event = Event::create($plan->title)
                ->startsAt(new DateTime($plan->dateTime), $plan->time !== null)
                ->status(
                    $plan->is_finished
                        ? EventStatus::cancelled()
                        : EventStatus::confirmed()
                );
            if ($plan->comment) {
                $event->description($plan->comment);
            }
            if ($plan->time) {
                $event->alertMinutesBefore(60, $plan->title);
            } else {
                $event->fullDay();
                $event->alertAt(new DateTime($plan->date . ' 05:00:00'), $plan->title);
            }
            $calendar->event($event);
        }
        return $calendar
            ->refreshInterval(5)
            ->get();
    }
}
