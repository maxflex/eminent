<?php

namespace App\Http\Controllers;

use App\Models\Payment;

class PaymentController extends Controller
{
    public function store()
    {
        Payment::create(['paid_at' => now()]);
    }
}
