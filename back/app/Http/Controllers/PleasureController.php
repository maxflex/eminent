<?php

namespace App\Http\Controllers;

use App\Http\Requests\PleasureRequest;
use App\Http\Resources\PleasureResource;
use App\Models\Pleasure;
use Illuminate\Http\Request;

class PleasureController extends Controller
{
    public function index(Request $request)
    {
        $query = Pleasure::query();
        return $this->handleIndexRequest($request, $query, PleasureResource::class);
    }

    public function store(PleasureRequest $request)
    {
        $pleasure = Pleasure::create($request->all());
        return new PleasureResource($pleasure);
    }

    public function update(Pleasure $pleasure, PleasureRequest $request)
    {
        $pleasure->update($request->all());
        return new PleasureResource($pleasure);
    }

    public function destroy(Pleasure $pleasure)
    {
        $pleasure->delete();
    }
}
