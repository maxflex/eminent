<?php

namespace App\Http\Controllers;

use App\Http\Resources\PenaltyListResource;
use App\Http\Resources\PleasureResource;
use App\Models\Penalty;
use App\Models\Pleasure;
use Illuminate\Http\Request;

class PenaltyController extends Controller
{
    public function index(Request $request)
    {
        $query = Penalty::debt()
            ->selectRaw("
                title,
                count(*) as cnt,
                cast(sum(penalty) as unsigned) as `sum`
            ")
            ->groupBy('title')
            ->orderBy('sum', 'desc');
        return $this->handleIndexRequest($request, $query, PenaltyListResource::class);
    }

    /**
     * Использовать удовольствие
     */
    public function store(Request $request)
    {
        $request->validate([
            'id' => ['required', 'exists:pleasures,id'],
            'yesterday' => ['required', 'boolean']
        ]);
        $pleasure = Pleasure::find($request->id);
        $pleasure->penalties()->create([
            'date' => $request->yesterday ? now()->modify('-1 day') : now(),
            'title' => $pleasure->title,
            'penalty' => $pleasure->is_free_available ? 0 : $pleasure->penalty
        ]);
        return new PleasureResource($pleasure);
    }
}
