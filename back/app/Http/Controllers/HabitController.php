<?php

namespace App\Http\Controllers;

use App\Http\Resources\HabitResource;
use App\Models\Habit;
use Illuminate\Http\Request;

class HabitController extends Controller
{
    public function index(Request $request)
    {
        $query = Habit::latest();
        return $this->handleIndexRequest($request, $query, HabitResource::class);
    }

    public function store(Request $request)
    {
        $habit = Habit::create($request->all());
        return new HabitResource($habit);
    }

    public function update(Habit $habit, Request $request)
    {
        $habit->update($request->all());
        return new HabitResource($habit);
    }

    public function destroy(Habit $habit)
    {
        $habit->delete();
    }
}
