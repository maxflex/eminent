<?php

namespace App\Http\Controllers;

use App\Http\Resources\HabitForTodayResource;
use App\Models\Habit;
use Illuminate\Http\Request;

class HabitForTodayController extends Controller
{
    public function index(Request $request)
    {
        $request->validate([
            'date' => ['required', 'string', 'date_format:Y-m-d']
        ]);

        return HabitForTodayResource::collection(
            Habit::getForToday($request->date)
        );
    }

    public function update($id, Request $request)
    {
        [$id, $date] = explode('|', $id);
        $habit = Habit::findOrFail($id);
        if ($request->is_finished) {
            $habit->logs()->create([
                'title' => $habit->title,
                'completed_at' => $date
            ]);
        } else {
            $habit->logs()
                ->where('completed_at', $date)
                ->first()
                ?->delete();
        }
    }
}
