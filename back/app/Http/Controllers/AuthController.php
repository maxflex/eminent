<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request->email)->first();
        if (!$user || !Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages(['email' => ['В доступе отказано']]);
        }
        return [
            'token' => $user->createToken($_SERVER['HTTP_USER_AGENT'])->plainTextToken,
            'user' => new UserResource($user),
        ];
    }

    public function user()
    {
        if (!auth('sanctum')->check()) {
            return response('', 401);
        }
        return [
            'user' => new UserResource(auth('sanctum')->user())
        ];
    }
}
