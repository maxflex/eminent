<?php

namespace App\Http\Controllers;

use App\Models\Habit;
use App\Models\Plan;
use App\Models\ShoppingList;

class CountsController extends Controller
{
    public function __invoke()
    {
        $today = now()->format('Y-m-d');
        $yesterday = now()->subDay()->format('Y-m-d');

        return [
            'plans' => Plan::where([
                'date' => $today,
                'is_finished' => false,
            ])->count(),

            'habits' => Habit::getForToday($yesterday)
                ->filter(fn($e) => !$e->is_finished && $e->date === $yesterday)
                ->count(),

            'shoppingLists' => ShoppingList::query()
                ->active()
                ->where('is_finished', false)
                ->where('priority', 1)
                ->count(),
        ];
    }
}
