<?php

namespace App\Http\Resources;

use App\Models\Habit;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Habit */
class HabitForTodayResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return extract_fields($this, [
            'date', 'is_finished', 'title', 'comment', 'streak',
            'period', 'daily_on'
        ], [
            'id' => join('|', [$this->id, $this->date])
        ]);
    }
}
