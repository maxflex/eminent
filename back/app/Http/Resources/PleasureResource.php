<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PleasureResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return extract_fields($this, [
            'title', 'comment', 'units', 'penalty', 'cooldown',
            'last_used_at', 'last_free_used_at', 'is_free_available',
            'current_streak', 'max_streak', 'is_streak_record', 'is_hidden'
        ]);
    }
}
