<?php

namespace App\Http\Resources;

use App\Models\Habit;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Habit
 */
class HabitResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return extract_fields($this, [
            'title', 'period', 'daily_on', 'created_at'
        ]);
    }
}
