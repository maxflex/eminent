<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{

    protected $fillable = [
        'title', 'comment', 'date', 'time', 'is_finished', 'is_important',
    ];

    protected $casts = [
        'is_finished' => 'bool',
        'is_important' => 'bool',
    ];

    public function penalties()
    {
        return $this->morphMany(Penalty::class, 'entity');
    }

    public function getTimeAttribute($value)
    {
        if (!$value) {
            return null;
        }
        return substr($value, 0, 5);
    }

    public function getDateTimeAttribute()
    {
        $result = [$this->date];
        if ($this->time) {
            $result[] = $this->time;
        }
        return join(' ', $result);
    }
}
