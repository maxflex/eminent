<?php

namespace App\Models;

use App\Enums\HabitPeriod;
use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

class Habit extends Model
{
    protected $fillable = [
        'daily_on', 'period', 'title',
    ];

    protected $casts = [
        'daily_on' => 'array',
        'period' => HabitPeriod::class
    ];

    public function logs(): HasMany
    {
        return $this->hasMany(HabitLog::class);
    }

    /**
     * Получить список привычек as for today
     *
     * @return Collection<int, object{date: string, is_finished: bool, streak: int}>
     */
    public static function getForToday(string $date): Collection
    {
        $now = CarbonImmutable::createFromFormat('Y-m-d', $date);
        $nowDayOfWeek = ($now->dayOfWeek + 6) % 7; // Adjust to 0 = Monday, 6 = Sunday
        $startOfWeek = $now->startOfWeek();
        $endOfWeek = $now->endOfWeek()->format('Y-m-d');
        $startOfMonth = $now->startOfMonth();
        $endOfMonth = $now->endOfMonth()->format('Y-m-d');
        $result = collect();

        // начиная с даты создания
        $habits = Habit::latest()->whereDate('created_at', '<=', $now)->get();

        foreach ($habits as $habit) {
            switch ($habit->period) {
                case HabitPeriod::monthly:
                    $habit->date = $endOfMonth;
                    $habit->is_finished = $habit->logs()->where(
                        'completed_at',
                        '>=',
                        $startOfMonth
                    )->exists();
                    $habit->streak = self::calculateStreak($habit, HabitPeriod::monthly, $now);
                    $result->push($habit);
                    break;

                case HabitPeriod::weekly:
                    $habit->date = $endOfWeek;
                    $habit->is_finished = $habit->logs()->where(
                        'completed_at',
                        '>=',
                        $startOfWeek
                    )->exists();
                    $habit->streak = self::calculateStreak($habit, HabitPeriod::weekly, $now);
                    $result->push($habit);
                    break;

                case HabitPeriod::daily:
                    if (in_array($nowDayOfWeek, $habit->daily_on)) {
                        $date = $now->format('Y-m-d');
                        $habit->date = $date;
                        $habit->is_finished = $habit->logs()->whereDate('completed_at', $habit->date)->exists();
                        $habit->streak = self::calculateStreak($habit, HabitPeriod::daily, $now);
                        $result->push($habit);
                    }
            }
        }
        return $result;
    }

    /**
     * Calculate the current streak for a habit up to a given date.
     *
     * @return object{current: int, ended: ?int}
     */
    private static function calculateStreak(
        Habit           $habit,
        HabitPeriod     $period,
        CarbonImmutable $date,
        bool            $getLastStreak = false
    ): object
    {
        $streak = 0;
        $currentDate = $date->copy()->subDays($getLastStreak ? 2 : 1);

        while (true) {
            $logExists = match ($period) {
                HabitPeriod::daily => $habit->logs()->whereDate('completed_at', $currentDate->format('Y-m-d'))->exists(),
                HabitPeriod::weekly => $habit->logs()->whereBetween('completed_at', [
                    $currentDate->startOfWeek()->format('Y-m-d'),
                    $currentDate->endOfWeek()->format('Y-m-d')
                ])->exists(),
                HabitPeriod::monthly => $habit->logs()->whereBetween('completed_at', [
                    $currentDate->startOfMonth()->format('Y-m-d'),
                    $currentDate->endOfMonth()->format('Y-m-d')
                ])->exists(),
                default => false,
            };

            if ($logExists) {
                $streak++;
                // Move back by one period
                $currentDate = match ($period) {
                    HabitPeriod::daily => $currentDate->subDay(),
                    HabitPeriod::weekly => $currentDate->subWeek(),
                    HabitPeriod::monthly => $currentDate->subMonth(),
                    default => $currentDate,
                };
            } else {
                break; // End of the streak
            }
        }

        return (object)[
            'current' => $streak,
            'ended' => $streak > 0 || $getLastStreak
                ? null
                : self::calculateStreak($habit, $period, $date, true)->current
        ];
    }
}
