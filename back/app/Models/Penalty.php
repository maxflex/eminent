<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Penalty extends Model
{
    protected $fillable = ['title', 'penalty', 'date'];

    public function scopeDebt($query)
    {
        return $query
            ->where('penalty', '>', 0)
            ->whereRaw("
                created_at > ifnull((select max(paid_at) from payments), 0)
            ");
    }
}
