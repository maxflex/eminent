<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Pleasure extends Model
{
    protected $fillable = [
        'title', 'comment', 'units', 'penalty', 'cooldown',
        'is_hidden'
    ];

    protected $casts = [
        'is_hidden' => 'boolean',
    ];

    public function penalties()
    {
        return $this->morphMany(Penalty::class, 'entity');
    }

    public function getLastUsedAtAttribute()
    {
        return $this->penalties()->max('date');
    }

    public function getLastFreeUsedAtAttribute()
    {
        return $this->penalties()->where('penalty', 0)->max('date');
    }

    /**
     * Сейчас рекорд по стрику
     */
    public function getIsStreakRecordAttribute(): bool
    {
        $currentStreak = $this->current_streak;
        return $currentStreak && $currentStreak >= $this->max_streak;
    }

    /**
     * Текущее воздержание (в днях)
     */
    public function getCurrentStreakAttribute(): null | int
    {
        $lastUsedAt = $this->last_used_at;
        if ($lastUsedAt === null) {
            return null;
        }
        return Carbon::parse($lastUsedAt)->diffInDays(now()->format('Y-m-d'));
    }

    /**
     * Максимальное воздержание (в днях)
     */
    public function getMaxStreakAttribute(): null | int
    {
        $dates = $this->penalties()->pluck('date');
        if (count($dates) === 0) {
            return null;
        }
        $maxStreak = -1;
        foreach ($dates as $index => $date) {
            $d = $index + 1 === count($dates) ? now()->format('Y-m-d') : $dates[$index + 1];
            $current = Carbon::parse($date)->diffInDays($d);
            if ($current > $maxStreak) {
                $maxStreak = $current;
            }
        }
        return $maxStreak;
    }

    public function getIsFreeAvailableAttribute()
    {
        if (!$this->cooldown) {
            return false;
        }
        $lastFreeUsedAt = $this->last_free_used_at;
        if ($lastFreeUsedAt === null) {
            return true;
        }
        $daysPassed = Carbon::parse($lastFreeUsedAt)->diffInDays(now()->format('Y-m-d'));
        return $daysPassed >= $this->cooldown;
    }
}
