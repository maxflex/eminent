<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShoppingList extends Model
{
    protected $fillable = [
        'title', 'comment', 'is_finished', 'priority'
    ];

    protected $casts = [
        'is_finished' => 'bool'
    ];

    /**
     * Завершённые висят неделю (и исчезают)
     * Активные висят месяц (и попадают в архив)
     */
    public function getIsArchivedAttribute(): bool
    {
        $archivePeriod = $this->is_finished
            ? now()->subWeek()
            : now()->subMonth();

        return $this->updated_at->lessThan($archivePeriod);
    }

    public function getIsImportantAttribute(): bool
    {
        return $this->priority === 1;
    }

    /**
     * Скрывать завершенные, с момента завершения которых прошло более недели
     */
    public function scopeActive($query)
    {
        $query->whereRaw("not (is_finished AND updated_at < now() - interval 1 week)");
    }
}
