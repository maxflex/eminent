<?php

namespace App\Enums;

enum HabitPeriod: string
{
    case daily = 'daily';
    case weekly = 'weekly';
    case monthly = 'monthly';
}