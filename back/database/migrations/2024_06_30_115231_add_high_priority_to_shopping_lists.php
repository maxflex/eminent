<?php

use App\Models\ShoppingList;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $lowPriorityIds = ShoppingList::where('is_low_priority', true)->pluck('id');
        $finishedIds = ShoppingList::where('is_finished', true)->pluck('id');

        Schema::table('shopping_lists', function (Blueprint $table) {
            $table->dateTime('finished_at')->nullable()->index()->after('comment');
            $table->tinyInteger('priority')->after('comment');
        });

        DB::table('shopping_lists')->whereIn('id', $lowPriorityIds)->update([
            'priority' => -1
        ]);

        DB::table('shopping_lists')->whereIn('id', $finishedIds)->update([
            'finished_at' => now()->format('Y-m-d H:i:s')
        ]);

        Schema::table('shopping_lists', function (Blueprint $table) {
            $table->dropColumn('is_low_priority');
            $table->dropColumn('is_finished');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('shopping_lists', function (Blueprint $table) {
            //
        });
    }
};
