<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $finishedIds = DB::table('shopping_lists')->whereNotNull('finished_at')->pluck('id');
        Schema::table('shopping_lists', function (Blueprint $table) {
            $table->dropColumn('finished_at');
        });
        Schema::table('shopping_lists', function (Blueprint $table) {
            $table->boolean('is_finished')->index()->default(false)->after('priority');
        });
        DB::table('shopping_lists')->whereIn('id', $finishedIds)->update([
            'is_finished' => true
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('shopping_lists', function (Blueprint $table) {
            //
        });
    }
};
